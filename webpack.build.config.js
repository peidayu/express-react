var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './client/common/',
    ],
    output: {
        path: path.join(__dirname, './client/dist'),
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!less-loader',
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader : 'file-loader',
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loaders: ['babel-loader'],
            }
        ],
        
    },
};
