var webpack = require('webpack');
var webpackMiddleware = require("webpack-dev-middleware");
var WebpackHotMiddleware = require('webpack-hot-middleware')
var webpackDevConfig = require('../webpack.config.js');
var compiler = webpack(webpackDevConfig);

module.exports = function(app) {
	app.use(webpackMiddleware(compiler, {
		publicPath: webpackDevConfig.output.publicPath,
		noInfo: true,
		stats: {
			colors: true
		}
	}));

	app.use(WebpackHotMiddleware(compiler, {
		log: console.log,
	}));
}