var express = require('express');
var webpackDev = require('./webpackDev');

var app = express();

app.set('views', './client');
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

console.log('process.env.NODE_ENV', process.env.NODE_ENV)
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('./client'));
} else {
  webpackDev(app);
}

app.get('/', function(req, res) {
    res.render('index')
});

app.get('/info', function(req, res) {
    var { version, arch, platform } = process;
    res.send({
        version: version,
        arch: arch,
        platform: platform,
        dirname: __dirname
    });
});

app.listen('1024', () => {console.log('servering......')})
