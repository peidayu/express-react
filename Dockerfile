FROM node:8-wheezy
RUN apt-get update \
    && apt-get install -y curl git\
    && mkdir -p /usr/apps \
    && cd /usr/apps \
    && git clone https://gitlab.com/peidayu/express-react.git \
    && cd express-react \
    && npm i \
    && npm run dev
